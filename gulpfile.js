/**
 *  by bublik
 *
 *  fast command to install
 *
 *  npm i --save-dev gulp gulp-browserify vinyl-source-stream gulp-sass gulp-autoprefixer gulp-pug2 browser-sync gulp-util chalk babelify babel-preset-es2015 babel-preset-react react react-dom gulp-jsbeautifier
 */


// project name for proxy
const projectName   = 'es-basement'

// gulp
const gulp          = require('gulp')

// es
const browserify    = require('browserify')
const babelify      = require('babelify')
const source        = require('vinyl-source-stream')

// sass
const sass          = require('gulp-sass')
const autoprefixer  = require('gulp-autoprefixer')

// pug
// const pug          = require('gulp-pug2')
// const prettify       = require('gulp-jsbeautifier')

// browser
const browserSync   = require('browser-sync').create()

// nicer browserify errors
const gutil         = require('gulp-util')
const chalk         = require('chalk')





// global path
const path = {

  proxy     : projectName + '/app/dist/',

  sassIn    : './app/src/sass/main.sass',
  sassOut   : './app/dist/css',

  pugIn     : './app/src/pug/*.pug',
  pugOut    : './app/dist',

  jsIn      : './app/src/js/app.js',
  jsOut     : './app/dist/js',

  watch     : {
    sass    : './app/src/**/*.sass',
    pug     : './app/src/**/*.pug',
    js      : './app/src/**/*.js',
    other   : [
                './app/**/*.php',
                './app/**/*.html'
              ]
  }
}





// sass
gulp.task('sass', () => {
  return gulp.src(path.sassIn)
    .pipe(sass({outputStyle: 'compressed'}).on('error', map_error))
    .pipe(autoprefixer())
    .pipe(gulp.dest(path.sassOut))
    .pipe(browserSync.stream())
})





// pug
// gulp.task('pug', () => {
//   return gulp.src(path.pugIn)
//     .pipe(pug({
//       yourTemplate: 'Locals',
//       pretty: true
//     }))
//     .on('error', map_error)
//     .pipe(prettify({
//       indent_level: 2,
//       indent_size : 2,
//       debug: true
//     }))
//     .pipe(gulp.dest(path.pugOut))
//     .pipe(browserSync.stream())
// })





// babel
gulp.task('babel', () => {
  return browserify(path.jsIn, {debug: true})
    .transform(babelify.configure({
      presets: ['es2015', 'react']
    }))
    .bundle()
    .on('error', map_error)
    .pipe(source('app.js'))
    .pipe(gulp.dest(path.jsOut))
    .pipe(browserSync.stream())
})





// reload
gulp.task('browser-sync', () => {
    browserSync.init({
        proxy: path.proxy
    })
})





// watch
gulp.task('watch', () => {
  gulp.watch(path.watch.sass, ['sass']  )
  // gulp.watch(path.watch.pug,  ['pug']   )
  gulp.watch(path.watch.js,   ['babel'] )
  gulp.watch(path.watch.other ).on('change', browserSync.reload)
})





// start gulp
gulp.task('default', ['sass', 'babel', 'browser-sync', 'watch'])





// nicer browserify errors
function map_error( err ) {
  if (err.fileName) {
    // regular error
    gutil.log(chalk.red(err.name)
      + ': '
      + chalk.yellow(err.fileName.replace(__dirname + '/app/src/', ''))
      + ': '
      + 'Line '
      + chalk.magenta(err.lineNumber)
      + ' & '
      + 'Column '
      + chalk.magenta(err.columnNumber || err.column)
      + ': '
      + chalk.blue(err.description))
  } else {
    // browserify error..
    gutil.log(chalk.red(err.name)
      + ': '
      + chalk.yellow(err.message))
  }

  this.emit('end')
}