import React from 'react'

class Span extends React.Component {

	render() {
		return(
			<span>
				{this.props.text}
			</span>
		)
	}

}

export { Span }