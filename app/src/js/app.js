import React from 'react'
import ReactDOM from 'react-dom'

import {Button} from './components/button'
import {dropdown} from './components/dropdown'

console.info(dropdown.text)

ReactDOM.render(
    <Button/>,
    document.querySelector('#root')
)